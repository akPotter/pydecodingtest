import requests,pyquery
import BeautifulSoup
from BeautifulSoup import UnicodeDammit
import sys, codecs
import encodings

encodings.aliases.aliases['gb2312']='gb18030'
encodings.aliases.aliases['gbk']='gb18030'

def zhBeautifulSoup(r_requests):
    fromreqEncoding = None
    if r_requests.encoding.lower()[0:3] != 'iso' and r_requests.encoding.lower()[0:3] != 'asc':
        fromreqEncoding = r_requests.encoding
    bu = BeautifulSoup.BeautifulSoup(r_requests.content,fromEncoding=fromreqEncoding)
    if bu.originalEncoding != bu.declaredHTMLEncoding and (bu.originalEncoding.lower()[0:3] == 'iso' or bu.originalEncoding.lower()[0:3] == 'asc'):
        if bu.declaredHTMLEncoding!= None and str(bu.declaredHTMLEncoding)!= 'None':
            r_unicodecontent = r_requests.content.decode(str(bu.declaredHTMLEncoding),'ignore').encode('utf-8')
            bu = BeautifulSoup.BeautifulSoup(r_unicodecontent,fromEncoding='utf-8')
    return bu

urllist = ["www.chinaren.com","www.chinanews.com.cn","www.chd.edu.cn","www.csic710.com.cn","www.jingji.com.cn","www.bjshrimp.cn","www.nim.ac.cn","www.orsi.ouc.edu.cn","pub.xaonline.com"]
#urllist = ["www.chinaren.com"]
reslist = []
bulist = []

outputstr = "[buHTMLencoding]\t[buOrigin]\t[bufromEncoding]\t[reqencoding]\t[reqapparent]\t[bustr]\t[destr]\r\n"

i=1
for url in urllist:
    destr = ""
    tempstr = u""
    r=requests.get("http://"+url)
    reslist.append(r)
    """
    if r.encoding.lower()[0:3] != 'iso' and r.encoding.lower()[0:3] != 'asc':
        fromreqEncoding = r.encoding
    bu = BeautifulSoup.BeautifulSoup(r.content,fromEncoding=fromreqEncoding)
    if bu.originalEncoding != bu.declaredHTMLEncoding and (bu.originalEncoding.lower()[0:3] == 'iso' or bu.originalEncoding.lower()[0:3] == 'asc'):
        if bu.declaredHTMLEncoding!= None and str(bu.declaredHTMLEncoding)!= 'None':
            r_unicodecontent = r.content.decode(str(bu.declaredHTMLEncoding),'ignore').encode('utf-8')
            bu2 = BeautifulSoup.BeautifulSoup(r_unicodecontent,fromEncoding='utf-8')
            destr = unicode(bu2.title.string).strip()
    """
    bu = zhBeautifulSoup(r)
    bulist.append(bu)
    #pyq = pyquery.PyQuery((r.text)
    tempstr = unicode(bu.declaredHTMLEncoding)+"\t"+unicode(bu.originalEncoding)+"\t"+unicode(bu.fromEncoding)+"\t"+unicode(r.encoding)+"\t"+unicode(r.apparent_encoding)+"\t"+unicode(bu.title.string).strip()+"\r\n"
    print i,tempstr,
    outputstr += tempstr
    i+=1
    #sys.exit(0)

#print outputstr
codecs.open("output.txt","w","utf-8").write(outputstr)
